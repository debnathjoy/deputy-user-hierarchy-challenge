package main

import (
	"bitbucket.org/debnathjoy/deputy-user-hierarchy-challenge/userhierarchy"
	"bufio"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

const PORT string = "7889"

type UserSubordinates struct {
	UserId       string
	Subordinates []userhierarchy.User
}

func handleSubordinates(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	var users []UserSubordinates
	if err != nil {
		fmt.Println(err)
	}
	ids, idExists := r.Form["id"]
	if idExists {
		for _, id := range ids {
			userId, _ := strconv.Atoi(id)
			users = append(users, UserSubordinates{
				UserId:       id,
				Subordinates: fetchUserSubordinates(userId),
			})
		}
		jsn, err := json.Marshal(users)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(jsn)
		return
	}
	http.Error(w, "Bad request", http.StatusBadRequest)
}

func handleWelcome(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Welcome to Deputy Coding Challenge Demo!")
}

func fetchUserSubordinates(userId int) []userhierarchy.User {
	// define all the roles
	roles := readRoleCSVFile()
	// define all the users
	users := readUserCSVFile()

	var uh userhierarchy.UserHierarchyFacade
	uh.SetRoles(roles)
	uh.SetUsers(users)
	subs, err := uh.GetSubordinates(userId)
	if err == nil {
		return subs
	}
	return nil
}

func readRoleCSVFile() []userhierarchy.Role {
	// Open the file
	csvfile, err := os.Open("roles.csv")
	var roles []userhierarchy.Role
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	// Parse the file
	r := csv.NewReader(bufio.NewReader(csvfile))

	// Iterate through the records
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		roleId, _ := strconv.Atoi(strings.TrimSpace(record[0]))
		parentId, _ := strconv.Atoi(strings.TrimSpace(record[2]))
		roles = append(roles, userhierarchy.Role{
			Id:     roleId,
			Name:   record[1],
			Parent: parentId,
		})
	}
	return roles
}

func readUserCSVFile() []userhierarchy.User {
	// Open the file
	csvfile, err := os.Open("users.csv")
	var users []userhierarchy.User
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	// Parse the file
	r := csv.NewReader(bufio.NewReader(csvfile))

	// Iterate through the records
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		userId, _ := strconv.Atoi(strings.TrimSpace(record[0]))
		roleId, _ := strconv.Atoi(strings.TrimSpace(record[2]))
		users = append(users, userhierarchy.User{
			Id:   userId,
			Name: record[1],
			Role: roleId,
		})
	}
	return users
}

func main() {
	http.HandleFunc("/", handleWelcome)
	http.HandleFunc("/subordinates/", handleSubordinates)
	err := http.ListenAndServe("localhost:"+PORT, nil)

	if err != nil {
		panic(err)
	}
}
