package test

import (
	"bitbucket.org/debnathjoy/deputy-user-hierarchy-challenge/userhierarchy"
	"reflect"
	"testing"
)

var singleton = make(map[int]userhierarchy.UserHierarchyFacade)

func getRoles() []userhierarchy.Role {
	return []userhierarchy.Role{
		{Id: 2, Name: "Location Manager", Parent: 1},
		{Id: 1, Name: "System Administrator", Parent: 0},
		{Id: 4, Name: "Employee", Parent: 3},
		{Id: 3, Name: "Supervisor", Parent: 2},
		{Id: 5, Name: "Trainer", Parent: 3},
	}
}

func getUsers() []userhierarchy.User {
	return []userhierarchy.User{
		{Id: 1, Name: "Adam Admin", Role: 1},
		{Id: 2, Name: "Emily Employee", Role: 4},
		{Id: 3, Name: "Sam Supervisor", Role: 3},
		{Id: 4, Name: "Mary Manager", Role: 2},
		{Id: 5, Name: "Steve Trainer", Role: 5},
	}
}

func getInstance() userhierarchy.UserHierarchyFacade {
	if _, found := singleton[1]; !found {
		roles := getRoles()
		users := getUsers()
		var uh userhierarchy.UserHierarchyFacade
		uh.SetRoles(roles)
		uh.SetUsers(users)
		singleton[1] = uh
	}
	return singleton[1]
}

func TestUniqueRoles(test *testing.T) {
	roles := getRoles()
	uniqueRoleIds := make(map[int]bool)
	for _, role := range roles {
		_, found := uniqueRoleIds[role.GetID()]
		if found {
			test.Error("Failed! Roles needs to have Unique Ids")
		} else {
			uniqueRoleIds[role.GetID()] = true
		}
	}
	if len(uniqueRoleIds) == 5 {
		test.Log("Roles have Unique Ids")
	}
}

func TestGetSubordinatesForUser1(test *testing.T) {
	uh := getInstance()
	userId1, err := uh.GetSubordinates(1)
	user1Subs := []userhierarchy.User{
		{Id: 2, Name: "Emily Employee", Role: 4},
		{Id: 5, Name: "Steve Trainer", Role: 5},
		{Id: 3, Name: "Sam Supervisor", Role: 3},
		{Id: 4, Name: "Mary Manager", Role: 2},
	}
	if err == nil {
		if reflect.DeepEqual(userId1, user1Subs) {
			test.Logf("User 1 has 4 subordinates: %+v", userId1)
		} else {
			test.Errorf("Failed! User 1 subordinates: %+v", userId1)
		}
	}
}

func TestGetSubordinatesForUser2(test *testing.T) {
	uh := getInstance()
	userId2, err := uh.GetSubordinates(2)
	if err == nil {
		if userId2 == nil {
			test.Log("User 2 does not have subordinates")
		} else {
			test.Errorf("Failed! User 2 subordinates: %+v", userId2)
		}
	}
}

func TestGetSubordinatesForUser3(test *testing.T) {
	uh := getInstance()
	userId3, err := uh.GetSubordinates(3)
	user3Subs := []userhierarchy.User{
		{Id: 2, Name: "Emily Employee", Role: 4},
		{Id: 5, Name: "Steve Trainer", Role: 5},
	}
	if err == nil {
		if reflect.DeepEqual(userId3, user3Subs) {
			test.Logf("User 3 has 2 subordinates: %+v", userId3)
		} else {
			test.Errorf("Failed! User 2 subordinates: %+v", userId3)
		}
	}
}
