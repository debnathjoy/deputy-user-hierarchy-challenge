## User Hierarchy

> Deputy Technical Interview - Coding Challenge Solution

## What it is

In Deputy system each user belongs to a user-group with a defined set of permissions. We name such a group "Role". A certain role (unless it is the root) must have a parent role to whom it reports to. 
Given a user Id, this package helps to returns a list of ALL their subordinates (i.e: including their subordinate's subordinates).

## Features

- Set All the System Roles
- Set All the System Users
- Get All Subordinates of a given user

## Documentation

Check out the [docs](https://jdebnath-readme-deputy.netlify.com) for in depth details about the problem and the solution architecture.
