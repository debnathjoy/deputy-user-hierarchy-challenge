package userhierarchy

import (
	"errors"
)

// UserHierarchyFacade Object
type UserHierarchyFacade struct {
	rc RoleCollection
	uc UserCollection
}

func (userHierarchy *UserHierarchyFacade) SetRoles(roles []Role) {
	userHierarchy.rc.SetRoles(roles)
}

func (userHierarchy *UserHierarchyFacade) SetUsers(users []User) {
	userHierarchy.uc.SetUsers(users)
	for _, user := range users {
		userHierarchy.rc.AddUserToUserGroup(user)
	}
}

// GetSubordinates gets all the subOrdinates all a given user ID
func (userHierarchy *UserHierarchyFacade) GetSubordinates(userID int) ([]User, error) {
	user, userExists := userHierarchy.uc.FindUser(userID)
	if userExists {
		return userHierarchy.rc.GetUserSubordinates(user)
	}
	return nil, errors.New("User ID does not exist")
}
