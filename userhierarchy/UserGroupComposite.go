package userhierarchy

import "fmt"

// UserGroupComposite interface that follows composite design pattern
type UserGroupComposite interface {
	String() string
	AppendChild(child UserGroup)
	AppendUser(user User)
	GetAllUsers(bool) []User
}

// UserGroup is the the implementation of UserGroupComposite
type UserGroup struct {
	name     string
	children []*UserGroup
	users    []User
	UserGroupComposite
}

func (usergroup *UserGroup) SetName(name string) {
	usergroup.name = name
}

func (usergroup *UserGroup) GetName() string {
	return usergroup.name
}

// AppendChild function
func (usergroup *UserGroup) AppendChild(child *UserGroup) bool {
	oldLen := len(usergroup.children)
	usergroup.children = append(usergroup.children, child)
	newLen := len(usergroup.children)
	return newLen > oldLen
}

// AppendUser function
func (usergroup *UserGroup) AppendUser(user User) bool {
	oldLen := len(usergroup.users)
	usergroup.users = append(usergroup.users, user)
	newLen := len(usergroup.users)
	return newLen > oldLen
}

// GetAllUsers gets subordinates of child roles
func (usergroup *UserGroup) GetAllUsers(ignoreRootUsers bool) []User {
	var users []User
	for _, value := range usergroup.children {
		users = append(users, value.GetAllUsers(false)...)
	}
	if !ignoreRootUsers {
		return append(users, usergroup.getUsers()...)
	}
	return users
}

// GetUsers gets users
func (usergroup *UserGroup) getUsers() []User {
	return usergroup.users
}

func (usergroup *UserGroup) String() string {
	return fmt.Sprintf("User group: %s, contains %d sub user-groups and %d users", usergroup.name, len(usergroup.children), len(usergroup.users))
}
