package userhierarchy

import "fmt"

// User Object
type User struct {
	Id   int
	Name string
	Role int
}

func (user *User) SetName(Name string) {
	user.Name = Name
}

func (user *User) SetID(ID int) {
	user.Id = ID
}

func (user *User) SetRole(Role int) {
	user.Role = Role
}

func (user *User) GetName() string {
	return user.Name
}

func (user *User) GetID() int {
	return user.Id
}

func (user *User) GetRole() int {
	return user.Role
}

func (user *User) String() string {
	return fmt.Sprintf("%v with Role ID %v", user.Name, user.Role)
}
