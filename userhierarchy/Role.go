package userhierarchy

// Role object
type Role struct {
	Id     int
	Name   string
	Parent int
}

func (role *Role) SetName(name string) {
	role.Name = name
}

func (role *Role) SetID(ID int) {
	role.Id = ID
}

func (role *Role) SetRole(parent int) {
	role.Parent = parent
}

func (role *Role) GetName() string {
	return role.Name
}

func (role *Role) GetID() int {
	return role.Id
}

func (role *Role) GetParent() int {
	return role.Parent
}
