package userhierarchy

// UserCollection object
type UserCollection struct {
	users         []User
	userHashTable map[int]User
}

func (uc *UserCollection) SetUsers(users []User) {
	uc.users = make([]User, len(users))
	uc.users = users
	// map the user in the hashtable
	uc.populateUserHashTable()
}

func (uc *UserCollection) GetUsers() []User {
	return uc.users
}

// PopulateUserHashTable creates userHashTable with user ID as key and the User object as value
// Given a user ID, the search capability will be faster (constant)
// PS: Assuming a user has only one role. Room for future improvement
func (uc *UserCollection) populateUserHashTable() {
	uc.userHashTable = make(map[int]User)
	for _, user := range uc.users {
		_, userExists := uc.userHashTable[user.GetID()]
		if !userExists {
			uc.userHashTable[user.GetID()] = user
		}
	}
}

// FindUser finds user from the hash table
func (uc *UserCollection) FindUser(userID int) (User, bool) {
	user, userExists := uc.userHashTable[userID]
	return user, userExists
}
