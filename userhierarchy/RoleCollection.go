package userhierarchy

import (
	"errors"
	"fmt"
)

// RoleCollection object
type RoleCollection struct {
	roles         []Role
	roleHashTable map[int]*UserGroup
}

func (rc *RoleCollection) SetRoles(roles []Role) {
	rc.roles = make([]Role, len(roles))
	rc.roles = roles
	// map the roles in the hashtable
	rc.populateRoleHashTable()
}

func (rc *RoleCollection) GetRoles() []Role {
	return rc.roles
}

// GetUserSubordinates find the User's UserGroup from the table based on User RoleId
// Then calls GetAllUsers on UserGroup type.
func (rc *RoleCollection) GetUserSubordinates(user User) ([]User, error) {
	role, roleFound := rc.roleHashTable[user.GetRole()]
	ignoreRootUsers := true
	if roleFound {
		return role.GetAllUsers(ignoreRootUsers), nil
	}
	return nil, errors.New("Role not found")
}

// Creates hashtable of UserGroup where key is the RoleID
// and value is UserGroup. Here UserGroup is a Composite Object
// which holds other UserGroup object as children
func (rc *RoleCollection) populateRoleHashTable() {
	// initialize the hashTable
	rc.roleHashTable = make(map[int]*UserGroup)
	for _, role := range rc.roles {
		userGroup, roleExists := rc.roleHashTable[role.GetID()]
		// if key does not exist in the hashtable then create the key
		// else update the name if the name was not set
		if roleExists {
			// if the groupName is empty, than update with proper name
			if userGroup.GetName() == "" {
				userGroup.SetName(role.GetName())
			}
		} else {
			rc.roleHashTable[role.GetID()] = &UserGroup{
				name: role.GetName(),
			}
		}

		// if parent exists, append UserGroup as child
		// else create a entry in the hashtable for parent
		_, parentExists := rc.roleHashTable[role.GetParent()]
		if parentExists {
			rc.roleHashTable[role.GetParent()].AppendChild(rc.roleHashTable[role.GetID()])
		} else {
			rc.roleHashTable[role.GetParent()] = &UserGroup{}
			rc.roleHashTable[role.GetParent()].AppendChild(rc.roleHashTable[role.GetID()])
		}
	}
}

// AddUserToUserGroup finds the user's UserGroup based on its role in roleHashTable
// and append the user in the UserGroup
func (rc *RoleCollection) AddUserToUserGroup(user User) {
	role, roleExists := rc.roleHashTable[user.GetRole()]
	if roleExists {
		role.AppendUser(user)
	} else {
		fmt.Printf("User Role => %d does not exists in the system", user.GetRole())
	}
}

//PrintRoleHashTable To check the content of the hashtable
func (rc *RoleCollection) PrintRoleHashTable() {
	for key, value := range rc.roleHashTable {
		if value.name != "" {
			fmt.Printf("address of %v is %p  \n", key, value)
		}
	}
}
